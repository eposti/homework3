import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class LongStack implements Cloneable {

   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }

   private LinkedList<Long> _list;

   LongStack() {
      this(new LinkedList<Long>());
   }

   LongStack(LinkedList<Long> list) {
      this._list = list;
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clonedLongStack = new LongStack((LinkedList<Long>) this._list.clone());
      return clonedLongStack;
   }

   public boolean stEmpty() {
      return _list.size() == 0;
   }

   public void push (long a) {
      _list.add(a);
   }

   public long pop() {
      return _list.removeLast();
   } // pop

   public void op (String s) {
      long item2 = this.pop();
      long item1 = this.pop();

      if(s.equals("+")) {
         this.push(item1 + item2);
         return;
      }
      if(s.equals("-")) {
         this.push(item1 - item2);
         return;
      }
      if(s.equals("*")) {
         this.push(item1 * item2);
         return;
      }
      if(s.equals("/")) {
         this.push(item1 / item2);
         return;
      }

      throw new RuntimeException(s);
   }
  
   public long tos() {

      return _list.getLast();
   }

   @Override
   public boolean equals (Object o) {

      return this._list.equals(((LongStack)o)._list);
   }

   @Override
   public String toString() {
      LinkedList<Long> reversedList = (LinkedList<Long>) _list.clone();
      Collections.reverse(reversedList);
      return _list.toString();
   }

   private static LinkedList<Long> findNumbers(String[] components) {
      LinkedList<Long> values = new LinkedList<Long>();

      for (String component : components) {
         try {
            Long number = Long.parseLong(component);
            values.add(number);
         } catch (NumberFormatException e) {}
      }

      return values;
   }

   private static boolean IsNumeric(String component) {
       try {
           Long number = Long.parseLong(component);
           return true;
       } catch (NumberFormatException e) {
           return false;
       }
   }

   private static LinkedList<String> findOperations(String[] components) {
      LinkedList<String> values = new LinkedList<String>();

      for (String component : components) {


         if("+*-/".contains(component)) {
            if(component.equals(components[0])) {
               throw new RuntimeException("Operand cant be first");
            }

            values.add(String.valueOf(component.charAt(0)));
         }
      }

      return values;
   }

   public static long interpret (String pol) {
      String[] components = pol.trim().split("\\s+");

      if(components.length == 0) {
         throw new RuntimeException("Avaldis on tühi");
      }

      LongStack resultStack = new LongStack();

      for (String component : components) {
          if(IsNumeric(component)) {
              Long number = Long.parseLong(component);
              resultStack.push(number);
          } else {
               try {
                   resultStack.op(component);
               } catch (NoSuchElementException e1) {
                  throw new RuntimeException("Avaldises " + pol + " pole piisavalt arve vastava tehte teostamiseks: " + component);
               } catch (RuntimeException e2) {
                   throw new RuntimeException("Avaldises " + pol + " sobimatu sümbol: " + component);
               }
          }
      }

      long result = resultStack.pop();

      if(!resultStack.stEmpty()) {
          throw new RuntimeException("Avaldises " + pol + " on liiga palju arve");
      }

      return result;
   }

}

